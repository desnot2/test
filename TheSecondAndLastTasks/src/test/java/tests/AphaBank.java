package tests;

import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Created by Юра on 23.06.2018.
 */
public class AphaBank {
    private static WebDriver driver = BeforeTest.setup();

    @Test
    public void testAlphaBank() throws InterruptedException {
        String searchSystem = driver.getCurrentUrl().substring(8,17);
        driver.findElement(By.xpath("/*//*[@id=\"text\"]")).sendKeys("Альфа-Банк");
        driver.findElement(By.xpath("/*//*[@id=\"text\"]")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("/html/body/div[3]/div[1]/div[2]/div[1]/div[1]/ul/li[2]/div/h2/a")).click(); //переход на сайт Альфа-банка

        Set<String> winSet = driver.getWindowHandles(); //получение списка урлов
        List<String> winList = new ArrayList<String>(winSet); //запись урлов в лист
        String newTab = winList.get(winList.size() - 1); //получение последней открытой вкладки
        driver.switchTo().window(newTab); //переключение на последнюю открытую вкладку

        driver.findElement(By.xpath("//*[@id=\"alfa\"]/div/div[4]/div/div[2]/div[1]/div/a[2]")).click(); //Вакансии
        driver.findElement(By.xpath("/html/body/div[1]/div/nav/nav/span[5]/a/span")).click(); // О работе в банке
        String text = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]")).getText();
        text = text + " " +  driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[4]")).getText(); //Запись текста в переменную
        writeToFile(searchSystem, text); //Создание файла и запись тескта
    }

    public void writeToFile(String searchSystem, String text)
    {
        Date date = new Date();
        SimpleDateFormat formatForDateNow = new SimpleDateFormat("yyyy.MM.dd - hh.mm.ss a zzz");
        Capabilities cap = ((RemoteWebDriver) driver).getCapabilities(); //получение имени браузера
        String browserName = cap.getBrowserName().toLowerCase();
        try (FileWriter fileWriter = new FileWriter(formatForDateNow.format(date) + " - " +  browserName + "-" + searchSystem +".txt")){
                fileWriter.write(text);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
