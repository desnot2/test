package tests;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by Юра on 02.04.2018.
 */
public class YandexMarket {
    private static WebDriver driver = BeforeTest.setup();

    @Test
    public void testSamsung() throws InterruptedException {
        driver.findElement(By.partialLinkText("Маркет")).click();
        driver.findElement(By.partialLinkText("Электроника")).click();
        driver.findElement(By.partialLinkText("Мобильные телефоны")).click();
        driver.findElement(By.xpath("//*[@id=\"search-prepack\"]/div/div/div[2]/div/div[1]/div[4]/fieldset/ul/li[10]/div/a/label")).click(); //фильтр Samsung
        driver.findElement(By.xpath("//*[@id=\"glpricefrom\"]")).sendKeys("40000"); //от 40000 руб.
        String firstName = driver.findElement(By.xpath("/html/body/div[1]/div[4]/div[2]/div[1]/div[2]/div/div[1]/div[1]/div[3]/div[2]/a")).getText(); //Запись имени первого смартфона
        System.out.println(firstName);
        driver.findElement(By.xpath("/html/body/div[1]/div[4]/div[2]/div[1]/div[2]/div/div[1]/div[1]/div[3]/div[2]/a")).click(); //Открытие ссылки для первой модели смартфона
        String secondName = driver.findElement(By.xpath("/html/body/div[1]/div[4]/div[2]/div[2]/div/div[1]/div[1]/div/h1")).getText(); //запись имени смартфона после перехода по ссылке
        System.out.println(secondName);
        if (firstName.equals(secondName)) { //Сравнение имен на совпадение
            System.out.println("Имена совпадают");
        }
        else
        {
            Assert.fail("имена не совпали");
        }

    }

    @Test
    public void testBeats() throws InterruptedException {
        driver.findElement(By.partialLinkText("Маркет")).click();
        driver.findElement(By.partialLinkText("Электроника")).click();
        driver.findElement(By.partialLinkText("Наушники и Bluetooth-гарнитуры")).click();
        driver.findElement(By.xpath("//*[@id=\"search-prepack\"]/div/div/div[2]/div/div[1]/div/div[3]/fieldset/ul/li[5]/div/a/label")).click(); //фильтр Beats
        driver.findElement(By.xpath("//*[@id=\"glpricefrom\"]")).sendKeys("17000"); //от 17000 руб.
        driver.findElement(By.xpath("//*[@id=\"glpriceto\"]")).sendKeys("25000"); // до 25000 руб.
        String firstName = driver.findElement(By.xpath("/html/body/div[1]/div[4]/div[2]/div[1]/div[2]/div/div[1]/div[1]/div[3]/div[2]/a")).getText(); //Запись имени первых наушников
        System.out.println(firstName);
        driver.findElement(By.xpath("/html/body/div[1]/div[4]/div[2]/div[1]/div[2]/div/div[1]/div[1]/div[3]/div[2]/a")).click(); // Открытие ссылки для первой модели наушников
        String secondName = driver.findElement(By.xpath("/html/body/div[1]/div[4]/div[2]/div[2]/div/div[1]/div[1]/div/h1")).getText(); //Запись имени наушников после перехода по ссылке
        System.out.println(secondName);
        if (firstName.equals(secondName)) { //Сравнение имен на совпадение
            System.out.println("Имена совпадают");
        }
        else
        {
            Assert.fail("имена не совпали");
        }
    }
}