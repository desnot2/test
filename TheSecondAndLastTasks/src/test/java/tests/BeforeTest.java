package tests;

import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by Юра on 23.06.2018.
 */
public class BeforeTest {
    @BeforeClass
    public static WebDriver setup() {
        WebDriver driver;
        System.setProperty("webdriver.chrome.driver", "D:\\projectsJava\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.get("https://yandex.ru");
        return driver;
    }
}
