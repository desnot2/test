import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by Юра on 23.06.2018.
 */
public class Sort {
    public static void main(String[] args) {
        try {
            FileReader fileReader = new FileReader("src/23.txt");
            String s = "";
            int c;
            while ((c = fileReader.read()) != -1) {

                s = s + ((char) c);
            }

            System.out.println("sortAscending");
            for (String ok : sortAscending(s)) {
                System.out.println(ok);
            }
            System.out.println("sortDescending");
            for (String ok : sortDescending(s)) {
                System.out.println(ok);
            }
            System.out.println("-----------TestOfString-----------");
            HashMap<String, Integer> map = symbolsOfString("aa  addddeee  ee");
            for (HashMap.Entry<String, Integer> item : map.entrySet()) {
                System.out.println(item.getKey() + " - " + item.getValue());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String[] sortDescending(String s) {
        String mass[] = new StringBuilder(s).toString().split(",");
        boolean isSorted = false;
        String buf;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < mass.length - 1; i++) {
                if (Integer.valueOf(mass[i]) < Integer.valueOf(mass[i + 1])) {
                    isSorted = false;
                    buf = mass[i];
                    mass[i] = mass[i + 1];
                    mass[i + 1] = buf;
                }

            }
        }
        return mass;
    }

    public static String[] sortAscending(String s) {
        String mass[] = new StringBuilder(s).toString().split(",");
        boolean isSorted = false;
        String buf;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < mass.length - 1; i++) {
                if (Integer.valueOf(mass[i]) > Integer.valueOf(mass[i + 1])) {
                    isSorted = false;
                    buf = mass[i];
                    mass[i] = mass[i + 1];
                    mass[i + 1] = buf;
                }

            }
        }
        return mass;
    }

    public static HashMap symbolsOfString(String stroka) {
        HashMap<String, Integer> result = new HashMap();
        String[] massive = stroka.split("");
        for (String character : massive) {
            if (result.containsKey(character)) {
                result.replace(character, result.get(character), result.get(character) + 1);
            } else {
                result.put(character, 1);
            }
        }
        return result;
    }
}
